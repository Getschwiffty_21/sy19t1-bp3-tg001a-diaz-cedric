/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/
#include "OgreManualObject.h"
#include "TutorialApplication.h"

void setParentNRevSpeed(Planet* planet, Planet* parent, int revSpeed)
{
	planet->setParent(parent);
	planet->setRevolutionSpeed(revSpeed);
}

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{ 
    // Create your scene here :)
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 1));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	sun = Planet::createPlanet(*mSceneMgr, 10, ColourValue::ColourValue(1, 1, 0, 1), "Sun");
	sun->setLocalRotationSpeed(1000);

	mercury = Planet::createPlanet(*mSceneMgr, 1.5f, ColourValue::ColourValue((float) 152/255, (float) 118/255, (float) 84/255, 1), "Mercury");
	mercury->setLocalRotationSpeed(1550);
	mercury->getNode().setPosition(Vector3::Vector3(150, 0, 0));
	setParentNRevSpeed(mercury, sun, 88);

	venus = Planet::createPlanet(*mSceneMgr, 2.5f, ColourValue::ColourValue((float) 254/255, (float) 127/255, (float) 156/255, 1), "Venus");
	venus->setLocalRotationSpeed(2550);
	venus->getNode().setPosition(Vector3::Vector3(250, 0, 0));
	setParentNRevSpeed(venus, sun, 224);

	earth = Planet::createPlanet(*mSceneMgr, 5, ColourValue::Green, "Earth");
	earth->setLocalRotationSpeed(2550);
	earth->getNode().setPosition(Vector3::Vector3(350, 0, 0));
	setParentNRevSpeed(earth, sun, 365);

	moon = Planet::createPlanet(*mSceneMgr, .5f, ColourValue::White, "Moon");
	moon->setLocalRotationSpeed(2550);
	moon->getNode().setPosition(Vector3::Vector3(350, 0, 0));
	setParentNRevSpeed(moon, earth, 1000);

	mars = Planet::createPlanet(*mSceneMgr, 4, ColourValue::Red, "Mars");
	mars->setLocalRotationSpeed(2150);
	mars->getNode().setPosition(Vector3::Vector3(450, 0, 0));
	setParentNRevSpeed(mars, sun, 687);

}


//Movement
bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	sun->update(evt);
	mercury->update(evt);
	venus->update(evt);
	earth->update(evt);
	moon->update(evt);
	mars->update(evt);
	return true;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
