#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace std;
using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* Node);
	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string name);

	void update(const FrameEvent& evt);
	SceneNode& getNode();
	void setParent(Planet* parent);
	Planet* getParent();
	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
};

