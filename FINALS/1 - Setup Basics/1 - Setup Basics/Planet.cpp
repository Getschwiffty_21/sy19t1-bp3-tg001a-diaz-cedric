#include "Planet.h"


Planet::Planet(SceneNode* Node)
{
	mNode = Node;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string name)
{
	SceneNode * node = sceneManager.getRootSceneNode()->createChildSceneNode();
	int rings = 20;
	int segments = 20;
	ManualObject* manual = sceneManager.createManualObject();


	if (name == "Sun")
	{
		manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
		manual->colour(colour);
	}
	else
	{
		MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");
		myManualObjectMaterial->getTechnique(0)->setDiffuse(colour);
		manual->begin(name, RenderOperation::OT_TRIANGLE_LIST);
		manual->colour(colour);
	}

	float fDeltaRingAngle = (Math::PI / rings);
	float fDeltaSegAngle = (2 * Math::PI / segments);
	int wVerticeIndex = 0;

	// Generate the group of rings for the sphere
	for (int mRings = 0; mRings <= rings; mRings++) {
		float r = size * sinf(mRings * fDeltaRingAngle);
		float y = size * cosf(mRings * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for (int seg = 0; seg <= segments; seg++) {
			float x = r * sinf(seg * fDeltaSegAngle);
			float z = r * cosf(seg * fDeltaSegAngle);

			// Add one vertex to the strip which makes up the sphere
			manual->position(x, y, z);
			manual->normal(Vector3(x, y, z).normalisedCopy());

			if (mRings != rings) {


				// each vertex (except the last) has six indicies pointing to it
				manual->index(wVerticeIndex + segments + 1);
				manual->index(wVerticeIndex);
				manual->index(wVerticeIndex + segments);
				manual->index(wVerticeIndex + segments + 1);
				manual->index(wVerticeIndex + 1);
				manual->index(wVerticeIndex);
				wVerticeIndex++;
			}
		}; // end for seg

	} // end for rings 

	manual->end();
	node->attachObject(manual);
	return new Planet(node);
}
	


void Planet::update(const FrameEvent& evt)
{
	Degree revSpd = Degree(365 / mRevolutionSpeed);
	Degree rot = Degree((mLocalRotationSpeed / 24) * evt.timeSinceLastFrame);
	mNode->rotate(Vector3::Vector3(0, 1, 0), Radian(rot));
	if (mParent != NULL)
	{
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
		Vector3 location = Vector3::ZERO;
		location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float oldX = location.x;
		float oldZ = location.z;
		float newX = (oldX * Math::Cos(planetRevolution)) + (oldZ * Math::Sin(planetRevolution));
		float newZ = (oldX * -Math::Sin(planetRevolution)) + (oldZ * Math::Cos(planetRevolution));

		newX += mParent->mNode->getPosition().x;
		newZ += mParent->mNode->getPosition().z;
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}
}

SceneNode& Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
