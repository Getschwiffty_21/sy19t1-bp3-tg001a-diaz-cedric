#include "Character.h"
#include"Skill.h"
#include"BasicAttack.h"
#include"Shockwave.h"
#include"Assassinate.h"
#include"Heal.h"

Character::Character(string teamName, string job, string name, int hp, int mp, int pow, int dex, int agi, int vit)
{
	mJob = job;
	mTeamName = teamName;
	mName = name;
	mHp = hp;
	mMaxHp = hp;
	mMp = mp;
	mPow = pow;
	mDex = dex;
	mAgi = agi;
	mVit = vit;

	mSkills.push_back(new BasicAttack("Basic Attack", 0, 1));
	if (job == "Mage")
	{
		mSkills.push_back(new Heal("Heal", 40, 0));
	}
	if (job == "Warrior")
	{
		mSkills.push_back(new Shockwave("Shockwave", 60, 0.9f));
	}
	if (job == "Assassin")
	{
		mSkills.push_back(new Assassinate("Assassinate", 30, 2.2f));
	}
}

void Character::move(Team* talon, Team* overwatch)
{

}

void Character::printStats()
{
	cout << "Class: " << mJob << endl; 
	cout << "Name: " << mName << endl;
	cout << "Hp: " << mHp << endl;
	cout << "Mp: " << mMp << endl;
	cout << "Pow: " << mPow << endl;
	cout << "Dex: " << mDex << endl;
	cout << "Agi: " << mAgi << endl;
	cout << "Vit: " << mVit << endl;
}

void Character::takeDamage(int value)
{
	if (value > 0)
	{
		mHp -= value;

		if (mHp < 0)
			mHp = 0;
	}
}

void Character::getHealed(int value)
{
	mHp += value;
}

void Character::reduceMp(int value)
{
	mMp -= value;
}

int Character::randomedPow()
{
	int how = mPow * 0.2;
	return mPow + rand() % how;
}


bool Character::isAlive()
{
	if (mHp > 0)
		return true;
	else
		return false;
}

string Character::getJob()
{
	return mJob;
}

//string Character::getJobStrength(string job)
//{
//	if (job == "warrior")
//	{
//
//	}
//}

string Character::getName()
{
	return mName;
}

string Character::getTeamName()
{
	return mTeamName;
}

int Character::getHp()
{
	return mHp;
}

int Character::getMaxHp()
{
	return mMaxHp;
}

int Character::getMp()
{
	return mMp;
}

int Character::getPow()
{
	return mPow;
}

int Character::getDex()
{
	return mDex;
}

int Character::getAgi()
{
	return mAgi;
}

int Character::getVit()
{
	return mVit;
}
