#pragma once
#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Character;
class Team
{
public:
	Team(string name, Character* member1, Character* member2, Character* member3);

	bool isAlive();
	void displayMembers();
	Character* teamlowestHp();
	string getName();
	vector<Character*> getTeam();
	int memberCount;

private:
	string mName;
	vector<Character*> mTeam;
};

