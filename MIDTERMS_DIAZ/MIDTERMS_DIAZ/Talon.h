#pragma once
#include"Character.h"

class Talon: public Character
{
public:

	Talon(string teamName, string job, string name, int hp, int mp, int pow, int dex, int agi, int vit);
	void move(Team* talon, Team* overwatch);
};

