#pragma once
#include"Skill.h"
#include"Team.h"

class Heal : public Skill
{
public:
	Heal(string name, int mpCost, int dmgeCoefficient);
	~Heal();

	void cast(Character* caster, Team* talon, Team* overwatch);
};


