#pragma once
#include"Skill.h"
#include"Team.h"

class Shockwave : public Skill
{
public:
	Shockwave(string name, int mpCost, int dmgeCoefficient);
	~Shockwave();

	void cast(Character* caster, Team* talon, Team* overwatch);
};

