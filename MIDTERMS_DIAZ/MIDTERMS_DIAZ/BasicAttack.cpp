#include "BasicAttack.h"
#include "Character.h"

BasicAttack::BasicAttack(string name, int mpCost, int dmgCoefficient) : Skill(name, mpCost, dmgCoefficient)
{

}

BasicAttack::~BasicAttack()
{

}

void BasicAttack::cast(Character* caster, Team* talon, Team* overwatch)
{
	Character* toAttack;
	int targetIndex;
	do 
	{ 
		targetIndex = rand() % 3; 
	} while (talon->getTeam()[targetIndex]->isAlive() == false);
	toAttack = talon->getTeam()[targetIndex];
	cout << caster->getName() << " uses " << mName << "!" << endl;
	caster->reduceMp(mMpCost);
	bool hit = isMiss(caster, toAttack);
	if (hit)
	{
		toAttack->takeDamage(generateDamage(caster, toAttack));
		cout << "...." << endl;
		cout << "Hits " << toAttack->getName() << "!"<< endl;
	}
	else
	{
		cout << "....Misses!" << endl;
	}
}

bool BasicAttack::isMiss(Character* caster, Character* target)
{
	int hitRate = (caster->getDex() / target->getAgi()) * 100;

	if (hitRate > 80)
	{
		hitRate = 80;
	}

	else if (hitRate < 20)
	{
		hitRate = 20;
	}

	if (rand() % 100 + 1 <= hitRate)
	{
		return true;
	}

	else
	{
		return false;
	}
}

