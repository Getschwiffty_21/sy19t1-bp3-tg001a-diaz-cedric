#include "Talon.h"

Talon::Talon(string teamName, string job, string name, int hp, int mp, int pow, int dex, int agi, int vit) : Character(teamName, job, name, hp, mp, pow, dex, agi, vit)
{

}

void Talon::move(Team* talon, Team* overwatch)
{
	int randomMove = rand() % 2;
	
	if (mMp >= mSkills[randomMove]->getMpCost()) {
		mSkills[randomMove]->cast(this, overwatch, talon);
	}
	else {
		mSkills[0]->cast(this, overwatch, talon);
	}
}
