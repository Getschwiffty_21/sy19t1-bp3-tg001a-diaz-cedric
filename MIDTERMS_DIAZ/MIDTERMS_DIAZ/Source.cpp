#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>
#include<vector>
#include"character.h"
#include"Team.h"
#include"Overwatch.h"
#include"Talon.h"

using namespace std;

vector<Character*> sortAgi(vector<Character*> members)
{
	for (int i = 0; i < members.size(); i++)
	{
		for (int j = 0; j < members.size() - 1; j++)
		{
			if (members[j]->getAgi() < members[j + 1]->getAgi())
				swap(members[j], members[j + 1]);
		}
	}
	return members;
}

void displayTurnOrder(vector<Character*> members)
{
	cout << "===============" << endl;
	for (int i = 0; i < 6; i++)
	{
		cout << members[i]->getName() << "'s AGI: " << members[i]->getAgi() << endl;
	}
}

void turn(vector<Character*> & characters)
{
	characters.push_back(characters[0]);
	characters.erase(characters.begin());

}

void removeCharacter(vector<Character*>& characters)
{
	for (int i = 0; i < characters.size(); i++) 
	{
		if (characters[i]->isAlive() == false)
		{
			characters.erase(characters.begin() + i);
		}
	}
}

void battleFlavorText()
{
	cout << R"(
            $$$$$$$$_\
           $$$ /  
$$$       $$$ / $$$$$$$$$$ \ 
 $$$     $$$ / $$$$    $$$$_\
  $$$   $$$ / $$$$         
   $$$ $$$ / $$$$$$$$$$$$ /
    $$$$$ /         $$$$ /
     $$$_/ $$$$    $$$$ /
            $$$$$$$$$$_/  
)" << endl;
}

void winFlavorText()
{
	cout << R"(
        ________________________________ _______           
|\     /\__   __(  ____ \__   __(  ___  (  ____ |\     /|  
| )   ( |  ) (  | (    \/  ) (  | (   ) | (    )( \   / )  
| |   | |  | |  | |        | |  | |   | | (____)|\ (_) /   
( (   ) )  | |  | |        | |  | |   | |     __) \   /    
 \ \_/ /   | |  | |        | |  | |   | | (\ (     ) (     
  \   / ___) (__| (____/\  | |  | (___) | ) \ \__  | |     
   \_/  \_______(_______/  )_(  (_______|/   \__/  \_/  
)" << endl;
}

void loseFlavorText()
{
	cout << R"(
 ______  _______ _______ _______ ________________
(  __  \(  ____ (  ____ (  ____ (  ___  \__   __/
| (  \  | (    \| (    \| (    \| (   ) |  ) (   
| |   ) | (__   | (__   | (__   | (___) |  | |   
| |   | |  __)  |  __)  |  __)  |  ___  |  | |   
| |   ) | (     | (     | (     | (   ) |  | |   
| (__/  | (____/| )     | (____/| )   ( |  | |   
(______/(_______|/      (_______|/     \|  )_(   
)" << endl;
}

void main()
{
	srand(time(NULL));

	Team* overwatch = new Team("OverWatch", new Overwatch("Overwatch", "Warrior", "Reinhardt", 45, 150, 16, 9, 10, 5), 
		new Overwatch("Overwatch", "Assassin", "Genji", 25, 100, 13, 10, 13, 3), 
		new Overwatch("Overwatch", "Mage", "Mercy", 15, 130, 14, 8, 10, 2));

	Team* talon = new Team("Talon", new Talon("Talon", "Warrior", "Sigma", 40, 160, 16, 10, 9, 4),
		new Talon("Talon", "Assassin", "Reaper", 30, 80, 14, 9, 14, 3),
		new Talon("Talon", "Mage", "Moira", 10, 110, 12, 11, 11, 1));

	overwatch->displayMembers();
	battleFlavorText();
	talon->displayMembers();

	vector<Character*> agiVector;
	for (int i = 0; i < 3; i++)
	{
		agiVector.push_back(overwatch->getTeam()[i]);
		agiVector.push_back(talon->getTeam()[i]);
	}

	agiVector = sortAgi(agiVector);
	displayTurnOrder(agiVector);
	_getch();
	system("CLS");

	vector<Character*>player = overwatch->getTeam();
	vector<Character*>enemy = talon->getTeam();
	
	while (talon->isAlive() && overwatch->isAlive())
	{
		agiVector[0]->printStats();
		agiVector[0]->move(talon, overwatch);
		turn(agiVector);

		removeCharacter(agiVector);
		_getch();
		system("CLS");
	}

	system("cls");
	if (overwatch->isAlive())
	{
		winFlavorText();
	}
	else
	{
		loseFlavorText();
	}
	system("pause");

}