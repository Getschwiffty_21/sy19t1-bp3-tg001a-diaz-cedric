#include "Assassinate.h"
#include "Character.h"

Assassinate::Assassinate(string name, int mpCost, int dmgeCoefficient) : Skill(name, mpCost, dmgeCoefficient)
{

}

Assassinate::~Assassinate()
{

}

void Assassinate::cast(Character* caster, Team* talon, Team* overwatch)
{
	Character* target = talon->teamlowestHp();
	cout << caster->getName() << " uses " << mName << " on " << target->getName() << "!" << endl;
	caster->reduceMp(mMpCost);
	target->takeDamage(generateDamage(caster, target));
}
