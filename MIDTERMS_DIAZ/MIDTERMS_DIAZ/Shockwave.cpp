#include "Shockwave.h"
#include "Character.h"

Shockwave::Shockwave(string name, int mpCost, int dmgeCoefficient) : Skill(name, mpCost, dmgeCoefficient)
{

}

Shockwave::~Shockwave()
{

}

void Shockwave::cast(Character* caster, Team* talon, Team* overwatch)
{
	cout << caster->getName() << " uses " << mName << "!" << endl;
	for (int i = 0; i < 3; i++)
	{
		if (talon->getTeam()[i]->isAlive())
		{
			talon->getTeam()[i]->takeDamage(generateDamage(caster, talon->getTeam()[i]));
		}
	}
	caster->reduceMp(mMpCost);
	
}