#pragma once
#include"Skill.h"
#include"Team.h"

class Assassinate : public Skill
{
public:
	Assassinate(string name, int mpCost, int dmgeCoefficient);
	~Assassinate();

	void cast(Character* caster, Team* talon, Team* overwatch);
};

