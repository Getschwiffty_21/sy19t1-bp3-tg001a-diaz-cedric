#include "Team.h"
#include "Character.h"

Team::Team(string name, Character* member1, Character* member2, Character* member3)
{
	mName = name;
	mTeam.push_back(member1);
	mTeam.push_back(member2);
	mTeam.push_back(member3);
}

bool Team::isAlive()
{
	memberCount = mTeam.size();
	for (int i = 0; i < mTeam.size(); i++)
	{
		if (mTeam[i]->isAlive() != true)
		{
			memberCount--;
		}
	}
	return memberCount > 0;
}

void Team::displayMembers()
{
	cout << "Team " << mName << endl;
	for (int i = 0; i < 3; i++)
	{
		if (mTeam[i]->isAlive()) 
		{
			cout << mTeam[i]->getJob() << ": " << mTeam[i]->getName() << endl;
		}
	}
}

Character* Team::teamlowestHp()
{
	int lowest = mTeam[0]->getHp();
	int place = 0;
	for (int i = 0; i < 3; i++) 
	{
		if (mTeam[i]->isAlive())
		{
			if (mTeam[i]->getHp() < lowest) 
			{
				lowest = mTeam[i]->getHp();
				place = i;
			}
		}
	}
	return mTeam[place];
}

string Team::getName()
{
	return mName;
}

vector<Character*> Team::getTeam()
{
	return mTeam;
}
