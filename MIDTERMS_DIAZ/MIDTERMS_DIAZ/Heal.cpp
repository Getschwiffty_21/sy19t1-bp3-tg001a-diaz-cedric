#include "Heal.h"
#include "Character.h"

Heal::Heal(string name, int mpCost, int dmgeCoefficient) : Skill(name, mpCost, dmgeCoefficient)
{

}

Heal::~Heal()
{

}

void Heal::cast(Character* caster, Team* talon, Team* overwatch)
{
	cout << caster->getName() << " uses " << mName << " on " << overwatch->teamlowestHp()->getName() << endl;
	caster->reduceMp(mMpCost);
	overwatch->teamlowestHp()->getHealed(overwatch->teamlowestHp()->getMaxHp() * 0.3);
}
