#pragma once
#include<iostream>
#include<string>
#include<vector>
#include "Skill.h"
#include"Team.h"
using namespace std;

class Character
{
public:
	Character(string teamName,string job, string name, int hp, int mp, int pow, int dex, int agi, int vit);

	virtual void move(Team* talon, Team* overwatch);
	void printStats();
	void takeDamage(int value);
	void getHealed(int value);
	void reduceMp(int value);
	int randomedPow();
	bool isAlive();

	string getJob();
	string getName();
	string getTeamName();
	int getHp();
	int getMaxHp();
	int getMp();
	int getPow();
	int getDex();
	int getAgi();
	int getVit();

protected:
	string mJob;
	string mJobStrength;
	string mTeamName;
	string mName;
	int mMaxHp;
	int mHp;
	int mMp;
	int mPow;
	int mDex;
	int mAgi;
	int mVit;
	vector<Skill*> mSkills;
};


