#include "Overwatch.h"

Overwatch::Overwatch(string teamName, string job, string name, int hp, int mp, int pow, int dex, int agi, int vit) : Character (teamName, job, name, hp, mp, pow, dex, agi, vit)
{

}

void Overwatch::move(Team* talon, Team* overwatch)
{
	cout << "0 = For Basic Attack" << endl;
	cout << "1 = For " << mSkills[1]->getName() << endl;
	int input;
	do 
	{
		cout << "Move: ";
		cin >> input;
		if (mSkills[input]->getMpCost() > mMp)
		{
			cout << "Insufficient MP to use " << mSkills[input]->getName() << "!" << endl;
		}
	} while (mSkills[input]->getMpCost() > mMp);
	mSkills[input]->cast(this, talon, overwatch);
}
