#pragma once
#include "Skill.h"
#include"Team.h"

class BasicAttack : public Skill
{
public:
	BasicAttack(string name, int mpCost, int dmgeCoefficient);
	~BasicAttack();

	void cast(Character* caster, Team* talon, Team* overwatch);
	bool isMiss(Character* caster, Character* target);
};

