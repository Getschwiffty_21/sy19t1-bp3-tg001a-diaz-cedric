#include "Skill.h"
#include "Character.h"

Skill::Skill(string name, int mpCost, int dmgCoefficient)
{
	mName = name;
	mMpCost = mpCost;
	damageCoefficient = dmgCoefficient;
}

void Skill::cast(Character* caster, Team* talon, Team* overwatch)
{

}

string Skill::getName()
{
	return mName;
}

int Skill::getMpCost()
{
	return mMpCost;
}

int Skill::generateDamage(Character* caster, Character* target)
{
	int baseDamage = (caster->randomedPow() * damageCoefficient);
	int damage = baseDamage - target->getVit();
	return damage;
}
