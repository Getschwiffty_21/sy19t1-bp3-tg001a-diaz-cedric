#pragma once
#include<iostream>
#include<string>
#include<vector>
#include"Team.h"

using namespace std;

class Character;
class Skill
{
public:
	Skill(string name, int mpCost, int dmgCoefficient);

	virtual void cast(Character* caster, Team* talon, Team* overwatch);
	// Talon is always the Target name
	// even if it's talon's turn to attack

	string getName();
	int getMpCost();
	int generateDamage(Character* caster, Character* target);

protected:
	string mName;
	int mMpCost;
	int damageCoefficient;
};

