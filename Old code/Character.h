#pragma once
#include <string>
#include <vector>
#include "Weapon.h"
#include "Skill.h"

using namespace std;
class Character
{
public:
	Character(string name, int hp, int mp, Weapon* weapon);
	
	string getInputName();
	int getInputHP();
	int getInputMP();


	void attack(Character *enemy);
private:
	string mName;
	int mMp;
	int mHp;
	Weapon *mWeapon;
	vector <Skill*> mSkills;
};

