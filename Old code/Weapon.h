#pragma once
#include <iostream>
#include <string>
using namespace std;

class Weapon
{
public:
	Weapon(string weaponName, int weaponDamage);

	string getInputName();
	int getInputDamage();

private:
	string mName;
	int mDamage;
};

