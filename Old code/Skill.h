#pragma once
#include <string>

using namespace std;

class Character; // Without the { }. This is a
// forward decleration.
class Skill
{
// anything that is in public can be ACCESSED outside
// the class. (i.e. in Source.cpp)
public:
	Skill();
	~Skill();
//A virtual keyword makes the function as something
//that can be "OVERRIDDEN". IE 
	virtual void cast(Character* target, Character *caster);

// anything that is in private CANNOT BE ACCESSED
// outside the class (I.e. in Source.cpp)
// We do this because we want to protect our variables
// from un-intentional changes from other programmers
//private: // CANNOT BE INHERITED

// protected is like private wherein it cannot be
// accessed outside. But CAN BE INHERITED
protected:
	string mName;
	int mpCost;


};

