#include "Weapon.h"
#include<stdlib.h>
#include<string>


Weapon::Weapon(string weaponName, int weaponDamage)
{
	this->mName = weaponName;
	this->mDamage = weaponDamage;
}

string Weapon::getInputName()
{
	return mName;
}

int Weapon::getInputDamage()
{
	return mDamage;
}
