#pragma once
#include "Skill.h"

// Inheritance Syntax
// This says "Syphon inherits from Skill"
// or "Syphon is a Skill"
class Syphon : public Skill
{
public:
	Syphon();
	~Syphon();

	void cast(Character* target, Character *caster);
};

