#pragma once
#include<iostream>
#include<string>

using namespace std;

class Player;
class Item
{
public:

	virtual void effect(Player* player);

	Item(string name, int effect, int counter);
	int getValue();
	string getName();

private:
	string mItemName;
	int mPoints;
	int mCounter;
};

