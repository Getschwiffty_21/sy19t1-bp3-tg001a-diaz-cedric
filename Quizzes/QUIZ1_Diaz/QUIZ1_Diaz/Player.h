#pragma once
#include<iostream>
#include<string>
#include<vector>

using namespace std;
class Item;
class Player
{
public:

	Player(string name, int hp, int rarityPoints, int crystals);

	void takeDamage(int value);
	void heal(int value);
	void addCrystals(int value);
	void addRarityPoints(int value);
	void printStats();
	void payUp();
	void win();
	void lose();
	void printItems();
	void addItems(Item* item);
	bool isAlive();
	bool canPay();

	string getName();
	int getHp();
	int getRarityPoints();
	int getCrystals();
	vector<Item*> getVectorItem();
private:
	string mName;
	int mHp;
	int mRarityPoints;
	int mCrystals;
	vector<Item*> items;
};

