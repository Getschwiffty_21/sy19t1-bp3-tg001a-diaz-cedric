#include "Crystal.h"
#include "Item.h"
#include"Player.h"

Crystal::Crystal(string name, int effect, int counter) : Item(name, effect, counter)
{
}

void Crystal::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->addCrystals(getValue());
	player->payUp();
}

