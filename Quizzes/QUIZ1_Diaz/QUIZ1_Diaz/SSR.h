#pragma once
#include "Item.h"
#include<iostream>
#include<string>

using namespace std;

class Player;
class SSR : public Item
{
public:
	SSR(string name, int effect, int counter);

	void effect(Player* player);
};

