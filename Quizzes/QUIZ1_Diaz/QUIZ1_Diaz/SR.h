#pragma once
#include "Item.h"
#include<iostream>
#include<string>

using namespace std;

class Player;
class SR : public Item
{
public:
	SR(string name, int effect, int counter);

	void effect(Player* player);
};

