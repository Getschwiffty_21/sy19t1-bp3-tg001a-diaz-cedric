#pragma once
#include "Item.h"
#include<iostream>
#include<string>

using namespace std;

class Player;
class Crystal : public Item
{
public:
	Crystal(string name, int effect, int counter);

	void effect(Player* player);
};

