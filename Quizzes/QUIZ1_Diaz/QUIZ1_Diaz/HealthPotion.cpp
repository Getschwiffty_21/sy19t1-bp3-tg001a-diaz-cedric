#include "HealthPotion.h"
#include "Item.h"
#include"Player.h"

HealthPotion::HealthPotion(string name, int effect, int counter) : Item(name, effect, counter)
{
}

void HealthPotion::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->heal(getValue());
	player->payUp();
}
