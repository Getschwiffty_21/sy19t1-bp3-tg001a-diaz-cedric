#pragma once
#include "Item.h"
#include<iostream>
#include<string>

using namespace std;

class Bomb : public Item
{
public:
	Bomb(string name, int effect, int counter);

	void effect(Player* player);
};

