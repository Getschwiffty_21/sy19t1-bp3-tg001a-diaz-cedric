#include "Bomb.h"
#include "Item.h"
#include"Player.h"

Bomb::Bomb(string name, int effect, int counter) : Item(name, effect, counter)
{
}

void Bomb::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->takeDamage(getValue());
	player->payUp();
}