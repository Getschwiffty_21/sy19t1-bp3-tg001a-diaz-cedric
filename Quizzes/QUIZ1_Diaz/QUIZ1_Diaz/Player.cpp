#include "Player.h"
#include "Item.h"



Player::Player(string name, int hp, int rarityPoints, int crystals)
{
	mName = name;
	mHp = hp;
	mRarityPoints = rarityPoints;
	mCrystals = crystals;
}

void Player::takeDamage(int value)
{
	if (value > 0)
	{
		mHp -= value;

		if (mHp < 0)
			mHp = 0;
	}
}
void Player::heal(int value)
{
	mHp += value;
}
void Player::addCrystals(int value)
{
	mCrystals += value;
}
void Player::addRarityPoints(int value)
{
	mRarityPoints += value;
}
void Player::printStats()
{
	cout << "Player: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Crystals: " << mCrystals << endl;
}

void Player::payUp()
{
	mCrystals -= 5;
}

void Player::win()
{
	if (mRarityPoints >= 100)
	cout << getName() << " wins!";
}

void Player::lose()
{
	if (!isAlive() || !canPay())
		cout << getName() << " lose!";
}

void Player::printItems()
{
	for (int i = 0; i < items.size(); i++)
	{
		cout << items[i]->getName() << endl;
	}
}

void Player::addItems(Item* item)
{
	items.push_back(item);
}

bool Player::isAlive()
{
	if (mHp > 0)
		return true;
	else
		return false;
}

bool Player::canPay()
{
	if (mCrystals > 0)
		return true;
	else
		return false;
}

string Player::getName()
{
	return mName;
}

int Player::getHp()
{
	return mHp;
}

int Player::getRarityPoints()
{
	return mRarityPoints;
}

int Player::getCrystals()
{ 
	return mCrystals;
}

vector<Item*> Player::getVectorItem()
{
	return items;
}
