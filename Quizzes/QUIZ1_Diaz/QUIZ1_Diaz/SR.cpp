#include "SR.h"
#include "Item.h"
#include"Player.h"

SR::SR(string name, int effect, int counter): Item(name, effect, counter)
{
}

void SR::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->addRarityPoints(getValue());
	player->payUp();
}
