#pragma once
#include<iostream>
#include<vector>
#include<string>

using namespace std;

class Player;
class Item;
class Gacha
{
public:
	Gacha();
	void roll(Player* player);
	
private:
	vector<Item*> mItems;
};

