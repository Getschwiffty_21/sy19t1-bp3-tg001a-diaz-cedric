#pragma once
#include "Item.h"
#include<iostream>
#include<string>

using namespace std;
class HealthPotion : public Item
{
public:
	HealthPotion(string name, int effect, int counter);

	void effect(Player* player);
};

