#include<iostream>
#include<string>
#include<conio.h>
#include<vector>
#include<time.h>
#include"Player.h"
#include"Item.h"
#include"Gacha.h"

using namespace std;

void main()
{
	srand(time(NULL));

	string name;
	cout << "Input name:";
	cin >> name;

	_getch();
	system("CLS");

	Player* player = new Player(name, 100, 0, 100);
	Gacha* gacha = new Gacha();
	
	while (player->isAlive() && player->canPay() && player->getRarityPoints() < 100)
	{
		player->printStats();
		gacha->roll(player);
		_getch();
	}

	player->win();
	player->lose();
	player->printItems();
	
	_getch();
}

