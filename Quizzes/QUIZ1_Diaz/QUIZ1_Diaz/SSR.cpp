#include "SSR.h"
#include "Item.h"
#include"Player.h"

SSR::SSR(string name, int effect, int counter): Item(name, effect, counter)
{
}

void SSR::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->addRarityPoints(getValue());
	player->payUp();
}
