#pragma once
#include "Item.h"
#include<iostream>
#include<string>

class R : public Item
{
public:
	R(string name, int effect, int counter);

	void effect(Player* player);
};

