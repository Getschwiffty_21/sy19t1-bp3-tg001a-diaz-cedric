#include "Gacha.h"
#include"Player.h"
#include"SSR.h"
#include"SR.h"
#include"R.h"
#include"Bomb.h"
#include"HealthPotion.h"
#include"Crystal.h"
#include "Item.h"

Gacha::Gacha()
{
	mItems.push_back(new SSR("SSR", 50, 1));

	for (int i = 0; i < 9; i++)
	{
		mItems.push_back(new SR("SR", 10, 1));
	}
	
	for (int i = 0; i < 40; i++)
	{
		mItems.push_back(new R("R", 1, 1));
	}
	
	for (int i = 0; i < 15; i++)
	{
		mItems.push_back(new HealthPotion("HealthPotion", 30, 1));
	}

	for (int i = 0; i < 20; i++)
	{
		mItems.push_back(new Bomb("Bomb", 25, 1));
	}
	
	for (int i = 0; i < 15; i++)
	{
		mItems.push_back(new Crystal("Crystal", 15, 1));
	}
}

void Gacha::roll(Player* player)
{
	if (player->canPay())
	{
		int randIndex = rand() % mItems.size();
		Item* itemToGet = mItems[randIndex];
		player->addItems(itemToGet);
		itemToGet->effect(player);
	}
}

