#include "R.h"
#include "Item.h"
#include"Player.h"

R::R(string name, int effect, int counter) : Item(name, effect, counter)
{
}

void R::effect(Player* player)
{
	cout << "got a " << getName() << endl;
	player->addRarityPoints(getValue());
	player->payUp();
}
