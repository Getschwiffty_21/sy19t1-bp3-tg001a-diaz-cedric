#include "Planet.h"


Planet::Planet(SceneNode* Node)
{
	mNode = Node;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour)
{
	ManualObject* object = sceneManager.createManualObject();
	
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//FRONT
	object->position(-size, size, size);
	object->colour(colour);

	object->position(-size, -size, size);
	object->colour(colour);
	object->position(size, -size, size);
	object->colour(colour);

	object->position(size, size, size);
	object->position(-size, size, size);
	object->position(size, -size, size);

	//RIGHT SIDE
	object->position(size, size, size);
	object->position(size, -size, size);
	object->position(size, -size, -size);

	object->position(size, -size, -size);
	object->position(size, size, -size);
	object->position(size, size, size);

	//BACK
	object->position(size, size, -size);
	object->position(size, -size, -size);
	object->position(-size, -size, -size);

	object->position(-size, -size, -size);
	object->position(-size, size, -size);
	object->position(size, size, -size);

	//LEFT SIDE
	object->position(-size, size, -size);
	object->position(-size, -size, -size);
	object->position(-size, -size, size);

	object->position(-size, -size, size);
	object->position(-size, size, size);
	object->position(-size, size, -size);

	//TOP
	object->position(-size, size, -size);
	object->position(-size, size, size);
	object->position(size, size, size);

	object->position(size, size, size);
	object->position(size, size, -size);
	object->position(-size, size, -size);

	//BACK
	object->position(-size, -size, -size);
	object->position(size, -size, -size);
	object->position(size, -size, size);

	object->position(size, -size, size);
	object->position(-size, -size, size);
	object->position(-size, -size, -size);


	object->end();
	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode();
	node->attachObject(object);
	return new Planet(node);
}

void Planet::update(const FrameEvent& evt)
{
	Degree revSpd = Degree(365 / mRevolutionSpeed);
	Degree rot = Degree((mLocalRotationSpeed / 24) * evt.timeSinceLastFrame);
	mNode->rotate(Vector3::Vector3(0, 1, 0), Radian(rot));
	if (mParent != NULL)
	{
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
		Vector3 location = Vector3::ZERO;
		location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float oldX = location.x;
		float oldZ = location.z;
		float newX = (oldX * Math::Cos(planetRevolution)) + (oldZ * Math::Sin(planetRevolution));
		float newZ = (oldX * -Math::Sin(planetRevolution)) + (oldZ * Math::Cos(planetRevolution));
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}
}

SceneNode& Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
