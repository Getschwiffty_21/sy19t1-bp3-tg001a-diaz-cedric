/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	int size = 20;
	int plot = size / 2;
	ManualObject* obj = mSceneMgr->createManualObject("cube");
	obj->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	std::vector<Vector3> positions = {
		Vector3::Vector3(plot, plot, plot),
		Vector3::Vector3(-plot, -plot, plot),
		Vector3::Vector3(plot, -plot, plot),
		Vector3::Vector3(-plot, plot, plot),
		Vector3::Vector3(plot, plot, -plot),
		Vector3::Vector3(plot, -plot, -plot),
		Vector3::Vector3(-plot, plot, -plot),
		Vector3::Vector3(-plot, -plot, -plot),
	};

	for (auto& x : positions) {
		obj->position(x);
	}

	obj->index(0);
	obj->index(1);
	obj->index(2);
	
	obj->index(3);
	obj->index(1);
	obj->index(0);

	
	obj->end();

	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(obj);

}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
