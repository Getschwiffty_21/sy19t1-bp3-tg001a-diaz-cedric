/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/
#include "OgreManualObject.h"
#include "TutorialApplication.h"

using namespace Ogre;
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	// Create object
	ManualObject* object = mSceneMgr->createManualObject("object1");
	// Begin drawing our object with white color, and no lighting
	// and using a triangle list (i.e. 3 vertices = 1 triangle)
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//FRONT
	object->position(-10, 10, 10);
	object->colour(ColourValue::Blue);

	object->position(-10, -10, 10);
	object->position(10, -10, 10);

	object->position(10, 10, 10);
	object->position(-10, 10, 10);
	object->position(10, -10, 10);

	//RIGHT SIDE
	object->position(10, 10, 10);
	object->position(10, -10, 10);
	object->position(10, -10, -10);

	object->position(10, -10, -10);
	object->position(10, 10, -10);
	object->position(10, 10, 10);
	
	//BACK
	object->position(10, 10, -10);
	object->position(10, -10, -10);
	object->position(-10, -10, -10);

	object->position(-10, -10, -10);
	object->position(-10, 10, -10);
	object->position(10, 10, -10);

	//LEFT SIDE
	object->position(-10, 10, -10);
	object->position(-10, -10, -10);
	object->position(-10, -10, 10);

	object->position(-10, -10, 10);
	object->position(-10, 10, 10);
	object->position(-10, 10, -10);

	//TOP
	object->position(-10, 10, -10);
	object->position(-10, 10, 10);
	object->position(10, 10, 10);

	object->position(10, 10, 10);
	object->position(10, 10, -10);
	object->position(-10, 10, -10);

	//BACK
	object->position(-10,-10,-10);
	object->position(10, -10, -10);
	object->position(10, -10, 10);

	object->position(10, -10, 10);
	object->position(-10, -10, 10);
	object->position(-10, -10, -10);


	object->end();
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
